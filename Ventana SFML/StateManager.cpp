#include "StateManager.h"
#include "Game.h"
#include "Menu.h"
#include "Creditos.h"

StateManager::StateManager(sf::RenderWindow *rw)
{
	window = rw;
	try {
		host.setHost("http://query.yahooapis.com");
		request.setUri("v1/public/yql?q=select%20item.condition.code%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22dallas%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
		response = host.sendRequest(request);
		data = nlohmann::json::parse(response.getBody().c_str());
		std::string weather = data["query"]["results"]["channel"]["item"]["condition"]["code"];
		weathercode = stoi(weather);
	}
	catch(std::exception error){
		weathercode = -1;
	}

	switch (weathercode) {
	case -1:
		bgcolor = sf::Color::Black;
		break;
	case 33:
		bgcolor = sf::Color::Cyan;
		break;
	case 34:
		bgcolor = sf::Color::Cyan;
		break;
	case 24:
		bgcolor = sf::Color::Blue;
		break;
	case 26:
		bgcolor = sf::Color::Green;
		break;
	case 36:
		bgcolor = sf::Color::Red;
		break;
	case 11:
		bgcolor = sf::Color::Magenta;
		break;
	case 10:
		bgcolor = sf::Color::Magenta;
		break;
	default:
		bgcolor = sf::Color::Black;
		break;
	}
}


StateManager::~StateManager()
{
}

void StateManager::Run(){

	Game *game;
	Menu *menu;
	Creditos *creditos;


	while (mode != 0){
		switch (mode){

		case 1:
			game = new Game();
			mode = game->Run(window, windowwidth, windowheight,bgcolor);
			delete game;
			break;

		case 2:
			menu = new Menu(window,bgcolor);
			mode = menu->Run(bgcolor);
			delete menu;
			break;

		case 3:
			creditos = new Creditos(window);
			mode = creditos->Run();
			delete creditos;
			break;

		}
	}
}