#include "Item.h"
#include "Includes.h"


Item::Item(sf::Texture tex, sf::RenderWindow *window)
{	
	itemsprite = new sf::Sprite(tex);
	rndw = window;
	Relocate();
	sb.loadFromFile("pickup.wav");
	pickup.setBuffer(sb);

	
}


Item::~Item()
{
	delete itemsprite;
}

void Item::Relocate(){
		srand(time(NULL));
		itemsprite->setPosition(rand() % rndw->getSize().x + 1, rand() % rndw->getSize().y + 1);
		pickup.play();
}

sf::Sprite Item::GetSprite()const{
	return *itemsprite;
}