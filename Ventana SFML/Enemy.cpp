#include "Enemy.h"
#include <iostream>


Enemy::Enemy(sf::Texture texture, sf::RenderWindow *window, int x, int y)
{
	sprite = new Sprite(texture);
	sprite->setPosition(x,y);
	sprite->setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(32, 32)));
	rw = window;
	std::cout << sprite->getTextureRect().height << std::endl;
	std::cout << sprite->getTextureRect().width << std::endl;
}

Enemy::~Enemy()
{
	delete sprite;
}

void Enemy::ChangeDirection(){
	left = !left;
}

void Enemy::ChangeDirection2(){
	if (dvdplayerbounce < 4){
		dvdplayerbounce++;
	}
	else{
		dvdplayerbounce = 0;
	}
}

void Enemy::DvdPlayerScreensaverMovement(){
	if (sprite->getPosition().x <= 0)
		ChangeDirection2();
	if (sprite->getPosition().x >= (rw->getSize().x - sprite->getGlobalBounds().width))
		ChangeDirection2();
	if (sprite->getPosition().y >= (rw->getSize().y - sprite->getGlobalBounds().height))
		ChangeDirection2();
	if (sprite->getPosition().y <= 0)
		ChangeDirection2();

	switch (dvdplayerbounce)
	{
	case 1:
		sprite->move(patrolspeed * elapsedtime, patrolspeed * elapsedtime);
		break;
	case 2:
		sprite->move(patrolspeed * elapsedtime, -patrolspeed * elapsedtime);
		break;
	case 3:
		sprite->move(-patrolspeed * elapsedtime, patrolspeed * elapsedtime);
		break;
	case 4:
		sprite->move(-patrolspeed * elapsedtime, -patrolspeed * elapsedtime);
		break;
	default:
		break;
	}
}

void Enemy::MoveLeftRight(){
	if (left){
		sprite->move(-patrolspeed * elapsedtime, 0);
	}
	else {
		sprite->move(patrolspeed * elapsedtime, 0);
	}
	if (sprite->getPosition().x <= 0)
		ChangeDirection();
	if (sprite->getPosition().x >= (rw->getSize().x - sprite->getGlobalBounds().width))
		ChangeDirection();
}
void Enemy::FollowPlayer(sf::Vector2f posplayer){
	if (posplayer.x > sprite->getPosition().x){
		sprite->move(speed * elapsedtime, 0);
	}
	if (posplayer.x < sprite->getPosition().x){
		sprite->move(-speed * elapsedtime, 0);
	}
	if (posplayer.y > sprite->getPosition().y){
		sprite->move(0, speed * elapsedtime);
	}
	if (posplayer.y < sprite->getPosition().y){
		sprite->move(0, -speed * elapsedtime);
	}
}
sf::Sprite* Enemy::GetSprite() const{
	return sprite;
}
void Enemy::UpdateElapsed(float newelapsed){
	elapsedtime = newelapsed;
}