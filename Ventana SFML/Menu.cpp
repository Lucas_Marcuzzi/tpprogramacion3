#include "Menu.h"
#include "Includes.h"


Menu::Menu(sf::RenderWindow *rw, sf::Color backgroundcolor)
{
	window = rw;
	window->clear(backgroundcolor);
	font.loadFromFile("Font.ttf");
	title.setFont(font);
	title.setCharacterSize(titlesize);
	title.setString("Cyberia");
	title.setPosition(window->getSize().x / 2 - title.getGlobalBounds().width / 2, window->getSize().y / 3);
	title.setFillColor(sf::Color::Green);
	play.setFont(font);
	play.setCharacterSize(optionsize);
	play.setString("Play");
	play.setPosition(window->getSize().x / 2 - play.getGlobalBounds().width / 2, window->getSize().y / 2);
	exit.setFont(font);
	exit.setCharacterSize(optionsize);
	exit.setString("Exit");
	exit.setPosition(window->getSize().x / 2 - exit.getGlobalBounds().width / 2, window->getSize().y / 2 + play.getGlobalBounds().height + 10);
	credits.setFont(font);
	credits.setCharacterSize(optionsize);
	credits.setString("Credits");
	credits.setPosition(window->getSize().x / 2 - credits.getGlobalBounds().width / 2, exit.getPosition().y + exit.getGlobalBounds().height + 10);
	cursortexture.loadFromFile("cursor.png");
	cursor.setTexture(cursortexture);
	cursor.setPosition(play.getPosition().x-cursorseparation-cursor.getGlobalBounds().width,play.getPosition().y);
	weather.setFont(font);
	weather.setCharacterSize(optionsize);
	if (backgroundcolor == sf::Color::Black) {
		weather.setString("The Weather is: Error");
	}
	if (backgroundcolor == sf::Color::Cyan) {
		weather.setString("The Weather is: Sunny");
	}
	if (backgroundcolor == sf::Color::Blue) {
		weather.setString("The Weather is: Windy");
	}
	if (backgroundcolor == sf::Color::Red) {
		weather.setString("The Weather is: Hot");
	}
	if (backgroundcolor == sf::Color::Green) {
		weather.setString("The Weather is: Cloudy");
	}
	if (backgroundcolor == sf::Color::Magenta) {
		weather.setString("The Weather is: Raining");
	}
}


Menu::~Menu()
{
}

int Menu::Run(sf::Color backgroundcolor){
	elapsed.restart();
	while (inmenu){

		if (keyboard.isKeyPressed(sf::Keyboard::Up)){
			if (option <= 1){
				option = 3;
			}
			else{
				option--;
			}
		}

		if (keyboard.isKeyPressed(sf::Keyboard::Down)){
			if (option >= 3){
				option = 1;
			}
			else{
				option++;
			}
		}

		switch (option)
		{
		case(1) :
			cursor.setPosition(play.getPosition().x - cursorseparation - cursor.getGlobalBounds().width, play.getPosition().y);
			break;
		case(2) :
			cursor.setPosition(exit.getPosition().x - cursorseparation - cursor.getGlobalBounds().width, exit.getPosition().y);
			break;
		case (3) :
			cursor.setPosition(credits.getPosition().x - cursorseparation - cursor.getGlobalBounds().width, credits.getPosition().y);
			break;
		default:

			break;
		}
		if (keyboard.isKeyPressed(sf::Keyboard::Space) && elapsed.getElapsedTime().asSeconds()>1){
			switch (option)
			{
			case(1) :
				window->clear(sf::Color::Black);
				inmenu = false;
				return 1;
				break;
			case(2) :
				window->clear(sf::Color::Black);
				inmenu = false;
				return 0;
				break;
			case (3) :
				window->clear(sf::Color::Black);
				inmenu = false;
				return 3;
				break;
			default:
				break;
			}
		}

		window->clear(backgroundcolor);
		window->draw(title);
		window->draw(play);
		window->draw(exit);
		window->draw(credits);
		window->draw(cursor);
		window->draw(weather);
		window->display();
	}
}