#pragma once
#include "Includes.h"
#ifndef GAME
#define GAME

class Game
{
private:
	sf::SoundBuffer sb;
	sf::Sound bgm;
	int fontsize = 50;
	bool ingame = true;
	const int defaultx = 350;
	const int defaulty = 0;
	sf::Sprite *hearts[3];
	int vidas = 3;
	int score = 0;
public:
	void SaveScore();
	void PlayerKill(sf::Sprite *player);
	bool Overlap(sf::Sprite* one, sf::Sprite* two)const;
	int Run(sf::RenderWindow *window, float windowwidth, float windowheight,sf::Color backgroundcolor);
	Game();
	~Game();
};
#endif

