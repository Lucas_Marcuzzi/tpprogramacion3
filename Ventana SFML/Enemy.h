#pragma once
#include "Includes.h"
#ifndef ENEMY
#define ENEMY

class Enemy : public sf::Sprite
{
private:
	int dvdplayerbounce = 1;
	const float speed = 85.0f;
	const float patrolspeed = 300.0f;
	sf::Sprite *sprite;
	bool left;
	float elapsedtime = 0;
	sf::RenderWindow *rw;

public:	
	Enemy(sf::Texture texture, sf::RenderWindow *window, int x, int y);
	~Enemy();
	void ChangeDirection();
	void MoveLeftRight();
	void FollowPlayer(sf::Vector2f playerpos);
	void Draw();
	void DvdPlayerScreensaverMovement();
	sf::Sprite* GetSprite() const;
	void ChangeDirection2();
	void UpdateElapsed(float newelapsed);
};

#endif