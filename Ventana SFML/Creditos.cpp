#include "Creditos.h"
#include "Includes.h"


Creditos::Creditos(sf::RenderWindow *rw)
{
	window = rw;
	fuente.loadFromFile("Font.ttf");
	sprites.setFont(fuente);
	sprites.setString("Art by Lucas Marcuzzi, Program used: Piskel.");
	sprites.setPosition(window->getSize().x / 2 - sprites.getLocalBounds().width / 2, window->getSize().y / 5);
	audio.setFont(fuente);
	audio.setString("Audio by Lucas Marcuzzi, Programs used: Bosca Ceoil and BFXR.");
	audio.setPosition(window->getSize().x / 2 - audio.getLocalBounds().width / 2, (window->getSize().y / 5)*2);
	codigo.setFont(fuente);
	codigo.setString("Coding by Lucas Marcuzzi.");
	codigo.setPosition(window->getSize().x / 2 - codigo.getLocalBounds().width / 2, (window->getSize().y / 5) * 3);
	librerias.setFont(fuente);
	librerias.setString("Libs used: SFML and Json for modern C++.");
	librerias.setPosition(window->getSize().x / 2 - librerias.getLocalBounds().width / 2, (window->getSize().y / 5) * 4);
	volver.setFont(fuente);
	volver.setString("To go back to menu press Space.");
	volver.setPosition(window->getSize().x / 2 - volver.getLocalBounds().width / 2, (window->getSize().y-volver.getGlobalBounds().height - 15));
}


Creditos::~Creditos()
{
}

int Creditos::Run(){
	elapsed.restart();
	window->clear(sf::Color::Black);
	window->draw(sprites);
	window->draw(audio);
	window->draw(codigo);
	window->draw(librerias);
	window->draw(volver);
	window->display();

	while (true){
		if (keyboard.isKeyPressed(sf::Keyboard::Space) && elapsed.getElapsedTime().asSeconds()>1){
			return 2;
		}
	}
}