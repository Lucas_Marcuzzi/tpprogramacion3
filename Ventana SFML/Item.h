#pragma once
#ifndef ITEM
#define ITEM
#include "Includes.h"
#include <time.h>
#include <random>
#include <stdlib.h> 

class Item
{
private:
	sf::SoundBuffer sb;
	sf::Sound pickup;
	sf::Sprite *itemsprite;
	sf::RenderWindow *rndw;
public:
	Item(sf::Texture tex, sf::RenderWindow *window);
	~Item();
	void Relocate();
	sf::Sprite GetSprite()const;
};
#endif

