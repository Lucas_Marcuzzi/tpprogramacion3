#pragma once
#ifndef CREDITOS
#define CREDITOS
#include "Includes.h"
#include "SFML\Graphics\Font.hpp"
#include "SFML\Graphics\Text.hpp"

class Creditos
{
private:
	sf::Clock elapsed;
	sf::Keyboard keyboard;
	sf::RenderWindow *window;
	sf::Font fuente;
	sf::Text sprites;
	sf::Text audio;
	sf::Text codigo;
	sf::Text librerias;
	sf::Text volver;
public:
	Creditos(sf::RenderWindow *rw);
	~Creditos();
	int Run();
};
#endif

