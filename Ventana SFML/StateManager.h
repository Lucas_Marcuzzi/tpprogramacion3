#ifndef STATEMANAGER
#define STATEMANAGER
#include "Includes.h"

class StateManager
{
private:
	int weathercode;
	sf::Color bgcolor;
	sf::Http host;
	sf::Http::Request request;
	sf::Http::Response response;
	nlohmann::json data;
	const float windowwidth = 1270.0f;
	const float windowheight = 720.0f;
	sf::RenderWindow *window;
	int mode = 2;
public:
	StateManager(sf::RenderWindow *rw);
	~StateManager();
	void Run();
};
#endif