#ifndef MENU
#define MENU
#include "Includes.h"
class Menu
{
private:
	sf::Text weather;
	sf::Clock elapsed;
	sf::Keyboard keyboard;
	sf::Text title;
	sf::Text play;
	sf::Text exit;
	sf::Text credits;
	sf::Font font;
	sf::Sprite cursor;
	sf::Texture cursortexture;
	int titlesize = 70;
	int optionsize = 30;
	int option = 1;
	float cursorseparation = 2.0f;
	bool inmenu = true;
	sf::RenderWindow *window;
public:
	Menu(sf::RenderWindow *rw, sf::Color backgroundcolor);
	~Menu();
	int Run(sf::Color backgroundcolor);
};
#endif

