#include "Includes.h"
#if DEBUG
//#include "vld.h"
#endif
#include "Game.h"
#include "Menu.h"
#include "StateManager.h"
#include <Windows.h>


void main(){
#if release
	FreeConsole();
#endif
	const float windowwidth = 1270.0f;
	const float windowheight = 720.0f;
	sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(windowwidth, windowheight), "NotAVirus.exe");
	
	StateManager *sm = new StateManager(window);
	sm->Run();

	delete sm;
	delete window;
}