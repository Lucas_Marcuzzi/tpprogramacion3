#include "Includes.h"
#include "Enemy.h"
#include "Item.h"
#include <conio.h> 


Game::Game()
{

}


Game::~Game()
{
}

bool Game::Overlap(sf::Sprite* one, sf::Sprite *two)const{
	if ((one->getPosition().x<two->getPosition().x && ((one->getPosition().x + one->getTextureRect().width) > two->getPosition().x))
		&& (one->getPosition().y < two->getPosition().y && ((one->getPosition().y + one->getTextureRect().height) > two->getPosition().y))){
		return true;
	}
	return false;
}

int Game::Run(sf::RenderWindow *window, float windowwidth, float windowheight, sf::Color backgroundcolor)
{
	
	sb.loadFromFile("bgm.wav");
	bgm.setBuffer(sb);
	bgm.setLoop(true);
	bgm.play();
	

	sf::Texture itemtexture;
	itemtexture.loadFromFile("item.png");
	Item *item = new Item(itemtexture,window);

	sf::Font font;
	font.loadFromFile("Font.ttf");
	ingame = true;
	const float enemywidth = 20.0f;

	sf::Keyboard keyboard;
	bool moveleft = false;
	float velocidad = 150.0f;

	sf::Texture charactertextures[4];
	charactertextures[0].loadFromFile("CharacterFront.png");
	charactertextures[1].loadFromFile("CharacterBack.png");
	charactertextures[2].loadFromFile("CharacterRight.png");
	charactertextures[3].loadFromFile("CharacterLeft.png");
	sf::Texture texture;
	sf::Texture enemytexture;
	enemytexture.loadFromFile("Virus.png");
	texture.loadFromFile("sprite.png");

	sf::Sprite *sprite = new sf::Sprite();
	sprite->setScale(2.0,2.0);
	sprite->setPosition(defaultx, defaulty);
	Enemy *chaser = new Enemy(texture, window, 0,0);
	Enemy *patrol = new Enemy(texture, window, 10, 200);
	Enemy *dvd = new Enemy(texture, window, 10,10);
	chaser->GetSprite()->setTexture(enemytexture);
	patrol->GetSprite()->setTexture(enemytexture);
	dvd->GetSprite()->setTexture(enemytexture);

	sf::Text lives("Lives :",font);
	lives.setPosition(sf::Vector2f(windowwidth-150, 0));
	lives.setFillColor(sf::Color::White);

	sf::Clock elapsed;
	float elapsedtime;

	sf::Text livescount(std::to_string(vidas),font);
	livescount.setPosition(sf::Vector2f(lives.getPosition().x + lives.getGlobalBounds().width + 10, 0));
	lives.setFillColor(sf::Color::White);

	sf::Text scoretext("Score :", font);
	scoretext.setPosition(sf::Vector2f(50, 0));
	scoretext.setFillColor(sf::Color::White);

	sf::Text scorecount(std::to_string(score), font);
	scorecount.setPosition(sf::Vector2f(scoretext.getPosition().x + scoretext.getGlobalBounds().width + 10, 0));
	scorecount.setFillColor(sf::Color::White);

	while (window->isOpen() && ingame)
	{
		elapsedtime = elapsed.getElapsedTime().asSeconds();
		elapsed.restart();

		dvd->UpdateElapsed(elapsedtime);
		patrol->UpdateElapsed(elapsedtime);
		chaser->UpdateElapsed(elapsedtime);

		dvd->DvdPlayerScreensaverMovement();
		patrol->MoveLeftRight();
		chaser->FollowPlayer(sprite->getPosition());

		if (keyboard.isKeyPressed(sf::Keyboard::Right)){
			sprite->move(velocidad * elapsedtime, 0);
			sprite->setTexture(charactertextures[2]);
		}
		if (keyboard.isKeyPressed(sf::Keyboard::Left)){
			sprite->move(-velocidad * elapsedtime, 0);
			sprite->setTexture(charactertextures[3]);
		}
		if (keyboard.isKeyPressed(sf::Keyboard::Up)){
			sprite->move(0, -velocidad * elapsedtime);
			sprite->setTexture(charactertextures[1]);
		}
		if (keyboard.isKeyPressed(sf::Keyboard::Down)){
			sprite->move(0, velocidad * elapsedtime);
			sprite->setTexture(charactertextures[0]);
		}
		if (keyboard.isKeyPressed(sf::Keyboard::Escape)){
			ingame = false;
			return 2;
		}

		//Checkeo de colisiones

		if (Overlap(chaser->GetSprite(), sprite))
			PlayerKill(sprite);
		if (Overlap(dvd->GetSprite(), sprite))
			PlayerKill(sprite);
		if (Overlap(patrol->GetSprite(), sprite))
			PlayerKill(sprite);
		if (Overlap(&item->GetSprite(), sprite)){
			item->Relocate();
			score += 10;
		}

		livescount.setString(std::to_string(vidas));
		scorecount.setString(std::to_string(score));

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window->pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window->close();
		}

		window->clear(backgroundcolor);
		window->draw(item->GetSprite());
		window->draw(*dvd->GetSprite());
		window->draw(*patrol->GetSprite());
		window->draw(*sprite);
		window->draw(*chaser->GetSprite());
		window->draw(lives);
		window->draw(livescount);
		window->draw(scoretext);
		window->draw(scorecount);
		window->display();
	}
	window->clear(backgroundcolor);
	delete chaser;
	delete patrol;
	delete dvd;
	delete sprite;
	delete item;
	bgm.stop();
	return 2;
}

void Game::PlayerKill(sf::Sprite *player){
	player->setPosition(defaultx, defaulty);
	vidas--;
	if (vidas <= 0)
		ingame = false;
}

/*void Game::SaveScore(){
	int cadena[100];
	FILE *pf;

	pf = fopen("score.txt", "w");
	for (int a = 1; a <= 3; a++)
	{
		printf(score);
		gets(cadena);

		fprintf(pf, " %snn", cadena);
	}
	fclose(pf);
}*/